$(document).ready(() => {
    $('#buttonWishes').click(function(event){
        $('#wish').empty();
        $('#wish').slideUp(100);
        event.preventDefault();
        $.ajax({
            method: 'GET',
            url: '/FAQ/DataWishes/',
            success: function(response){
                console.log(response)
                var datajson = JSON.parse(response);
                var index_random = Math.floor(Math.random() * datajson.length);
                var random_wish = "''" + datajson[index_random].fields.wishes + "''";
                $('#wish').html(random_wish)
                $('#wish').slideDown(500);
            }
        })
    })

    $('#submitFormWishes').click(function(event){
        var wishes = $('#inputWishes').val();
        event.preventDefault();
        console.log(wishes);
        $('#inputWishes').val('');
        $.ajax({
            method: 'POST',
            url: '/FAQ/submitWishes/',
            datatype:'json',
            data: {
                wishes: wishes, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
            success: function(data){
            if (data['success']) {
                $('#alert').html("Wishes are successfully added!");
                
            } else {
                $('#alert').html("Wishes not valid!");
                $('#quoteInput').empty();
            }
                
            }
        })
        console.log(wishes)

    })
})