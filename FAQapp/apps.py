from django.apps import AppConfig


class FaqappConfig(AppConfig):
    name = 'FAQapp'
