from django.test import TestCase
from django.test import Client
from .models import modelQuestion,modelWishes
from .views import forms_FAQ,FAQ
from .forms import formsFAQ
from django.http import HttpRequest
from django.contrib.auth.models import User

# Create your tests here.

class testFAQ(TestCase):
    def test_url_FAQ_exist(self):
        response = Client().get("/FAQ/")
        self.assertEquals(response.status_code, 200)
    
    def test_FAQ_template(self):
        response = Client().get("/FAQ/")
        self.assertTemplateUsed(response,'FAQ.html')

    def test_view_FAQ(self):
        response = Client().get("/FAQ/")
        isi_html = response.content.decode("utf8")
        self.assertIn('<title>FightCovid19: Frequently Asked Questions<', isi_html)
        self.assertIn('<div class="button">', isi_html)

    def test_url_forms_FAQ_exist(self):
        response = Client().get("/FAQ/form-FAQ/")
        self.assertEquals(response.status_code, 200)

    def test_forms_FAQ_template(self):
        response = Client().get("/FAQ/form-FAQ/")
        self.assertTemplateUsed(response,'forms_FAQ.html')
    
    def test_models_question_create(self):
        modelQuestion.objects.create(name="budi",email="budi@gmail.com",question="tanya apa?",answer="-")
        total = modelQuestion.objects.all().count()
        self.assertEquals(total, 1)

    def test_view_forms_FAQ(self):
        response = Client().get("/FAQ/form-FAQ/")
        isi_html = response.content.decode("utf8")
        self.assertIn('<title>FAQ: Ask Your Question!<', isi_html)
        self.assertIn('>LOGIN NOW', isi_html)
        
    def test_valid_post_method(self):
        form = formsFAQ(data={'name':'budi','email':'budi@gmail.com','question':'tanya?'})
        self.assertTrue(form.is_valid())
        response = self.client.post('/FAQ/',{'name':'budi','email':'budi@gmail.com','question':'tanya?'})
        self.assertEqual(response.status_code, 200)

    def test_blank_input(self):
        form = formsFAQ(data = {'name':'','email':'','question':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['name'], ["This field is required."])
        self.assertEqual(form.errors['email'], ["This field is required."])
        self.assertEqual(form.errors['question'], ["This field is required."])


    def test_str_model_to_html(self):
        tanya = modelQuestion.objects.create(name='budi',email='budi@gmail.com',question='tanya?',answer='-')
        response = Client().get('/FAQ/')
        isi_html = response.content.decode('utf8')
        self.assertIn(str(tanya), isi_html)

    #newtest 
    def test_url_submitWishes_DataWishes_exist(self):
        response1 = Client().get("/FAQ/submitWishes/")
        response2 = Client().get("/FAQ/DataWishes/")
        self.assertEquals(response1.status_code, 200)
        self.assertEquals(response2.status_code, 200)

    def test_models_wishes_create(self):
        modelWishes.objects.create(wishes="km pasti bisak")
        total = modelWishes.objects.all().count()
        self.assertEquals(total, 1)
    
    def test_create_wishes(self):
        wish = modelWishes.objects.create(wishes="wyatb")
        self.assertEqual("wyatb", str(wish))

    

    
