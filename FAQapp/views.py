from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import formsFAQ
from .models import modelQuestion
from FAQapp.models import modelWishes
from django.http import JsonResponse
#from django.db.models.fields import json
from django.core import serializers #load serializers to serialize Model data to JSON


# Create your views here.

def forms_FAQ(request):
    if request.method == 'POST':
        form = formsFAQ(request.POST or None)
        if form.is_valid():
            form.save()
            objectFAQ = modelQuestion.objects.all()
            return HttpResponseRedirect(redirect_to='/FAQ/')
        else:
            objectFAQ = modelQuestion.objects.all()
            return render(request, 'forms_FAQ.html', {'form': form,'database': objectFAQ})
    else:
        form = formsFAQ()
        objectFAQ = modelQuestion.objects.all()
        return render(request, 'forms_FAQ.html', {'form': form,'database': objectFAQ})

def FAQ(request):
    objectFAQ = modelQuestion.objects.all()
    return render(request, 'FAQ.html', {'database': objectFAQ})

def SubmitWishes(request):
    data = {'success':False}
    if request.method == 'POST':
        data_wishes = modelWishes()
        data_wishes.wishes = request.POST.get('wishes')
        data_wishes.idUser = request.user
        data_wishes.save()
        data['success'] = True
    return JsonResponse(data, safe=False)

def DataWishes(request):
    modelData = modelWishes.objects.all()
    data_json = serializers.serialize('json',modelData)
    return JsonResponse(data_json, safe=False)