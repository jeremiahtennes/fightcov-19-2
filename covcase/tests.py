from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from users.views import login_page, signup_page, logout_acc
from .models import Covid
from . import views
from .forms import kasusForm

class UnitTestCovCase(TestCase):
    
    def test_response_page(self):
        response = Client().get('/covcase/post')
        self.assertEqual(response.status_code,302)
    
    def test_url_listCase(self):
        response = Client().get('/covcase/listCase/')
        self.assertEquals(response.status_code, 200)

    def test_template1_used(self):
        response = Client().get('/covcase/')
        self.assertTemplateUsed(response, 'addCase.html')

    
    def test_template2_used(self):
        response = Client().get('/covcase/')
        self.assertTemplateUsed(response, 'base_layout.html')
    
    def test_func_page(self):
        found = resolve('/covcase/')
        self.assertEqual(found.func, views.covcase)

    def test_covcase_model_Covid(self):
        Covid.objects.create(Daerah = "Banten", Aktif=100, Sembuh=50, Death=40)
        total = Covid.objects.all().count()
        self.assertEquals(total, 1)
    
    def test_valid_postCov_method(self):
        form = kasusForm(data = {'Daerah':'Banten', 'Aktif':100,'Sembuh':50,'Death':40})
        self.assertTrue(form.is_valid())
        response = self.client.post('/covcase/listCase/')
        self.assertEqual(response.status_code,200)
    
    def test_konten_sebelum_login(self):
        response = Client().get('/covcase/')
        isi = response.content.decode('utf8')
        self.assertIn('Untuk mengakses form untuk menambahkan laporan kasus', isi)
    
    def test_konten_setelah_login(self):
        user = User.objects.create(username='tu')
        user.set_password('tp')
        user.save()
        self.client.login(username='tu', password='tp')
        form = kasusForm(data = {'Daerah':'Banten', 'Aktif':100,'Sembuh':50,'Death':40})
        self.assertTrue(form.is_valid())
        response = self.client.post('/covcase/listCase/')
        self.assertEqual(response.status_code,200)

    def test_modelCov_to_html(self):
        user = User.objects.create(username='tu')
        user.set_password('tp')
        user.save()
        self.client.login(username='tu', password='tp')
        cases = Covid.objects.create(Daerah = "Banten", Aktif=100, Sembuh=50, Death=40)
        response = Client().get('/covcase/listCase/')
        isi_html = response.content.decode('utf8')
        self.assertIn(str(cases), isi_html)
    
    def test_delete_setelah_login(self):
        user = User.objects.create(username='tu')
        user.set_password('tp')
        user.save()
        self.client.login(username='tu', password='tp')
        cases = Covid.objects.create(Daerah = "Banten", Aktif=100, Sembuh=50, Death=40)
        cases.delete()
    
    def test_delete_biasa(self):
        Client().post('/covcase/delete/<str:nama_daerah>', data={'nama_daerah': 'banten'})
        hapus = Covid.objects.filter(Daerah='banten').count()
        self.assertEqual(hapus, 0)
    
    def test_form_in_views_satu(self):
        obj = Covid.objects.create(Daerah='banten', Aktif=100, Sembuh=50, Death=40)
        Client().post('/covcase/post')
        tes = Covid.objects.count()
        self.assertEqual(tes, 1)
    
    def test_form_in_views_dua(self):
        obj = Covid.objects.create(Daerah='banten', Aktif=100, Sembuh=50, Death=40)
        Client().post('/covcase/')
        tes = Covid.objects.count()
        self.assertEqual(tes, 1)

        
    

    
