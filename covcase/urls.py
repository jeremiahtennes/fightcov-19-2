from django.urls import path
from . import views

app_name = 'covcase'

urlpatterns = [
    path('', views.covcase, name = "addCovcase_url"),
    path('post', views.post_covcase),
    path('delete/<str:nama_daerah>',  views.delete_case),
    path('listCase/',  views.listCase),
    path('dataCovJson', views.dataCovidJson),
]
