from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Covid (models.Model):
    Daerah = models.CharField(max_length=120)
    Aktif = models.IntegerField()
    Sembuh = models.IntegerField()
    Death = models.IntegerField()
   

    def __str__(self):
        return self.Daerah

