from django import forms

class kasusForm(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    # attrs = {
    #     'class': 'form-control', 'id':'form'
    # }
    Daerah = forms.CharField(widget=forms.TextInput(attrs= {'class': 'form-control', 'id':'Daerah'}), label='Daerah' , max_length=50, required=True)
    Aktif = forms.IntegerField(widget=forms.NumberInput(attrs= {'class': 'form-control', 'id':'Aktif'}), label='Aktif Kasus', required=True)
    Sembuh = forms.IntegerField(widget=forms.NumberInput(attrs= {'class': 'form-control', 'id':'Sembuh'}), label='Angka Kesembuhan', required=True)
    Death = forms.IntegerField(widget=forms.NumberInput(attrs= {'class': 'form-control', 'id':'Death'}), label='Angka Kematian ', required=True)
   
