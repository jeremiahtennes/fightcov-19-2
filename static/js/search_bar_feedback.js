$(document).ready(() => {
    $('#search').keyup(function () {
        var query = $('#search').val();
        var getUrl = '';

        if (query.length == 0) {
            getUrl = 'http://fightcov-19-2.herokuapp.com/feedback/getDataAll/'
        } else {
            getUrl = 'http://fightcov-19-2.herokuapp.com/feedback/getData/?q=' + query
        }

        console.log('query = ' + query + ', panjang = ' + query.length);
        console.log('url = ' + getUrl);

        $.ajax({
            method: 'GET',
            url: getUrl,
            success: function (response) {
                console.log(response);

                var object_result = $("#result");
                object_result.empty();

                if (response.length == 0) {
                    var trHTML = '<div style="margin: 30px;">';
                    trHTML += '<div style="text-align: center;"><h3 style="font-size: 20px;">No feedback author names found!</h3></div></div>';
                    trHTML += '<hr style="height:2px; border-width:0; color:gray; background-color:gray">'

                    $('#result').append(trHTML);

                } else {
                    var trHTML = '<div class="flex-wrap">';
                    for (let i = 0; i < response.length; i++) {
                        name = response[i].fields.name;
                        email = response[i].fields.email;
                        message = response[i].fields.message;
                        trHTML += '<div class="card">'
                        trHTML += '<div class="name">'
                        trHTML += '<h4 style="text-align: center;">' + name + '</h4>'
                        trHTML += '</div>'
                        trHTML += '<div class="email">'
                        trHTML += '<h6 style="text-align: center;">' + email + '</h4>'
                        trHTML += '</div>'
                        trHTML += '<a href="{% url \'feedback:delete_feedback\' id=' + response[i].pk + ' %}"><i class="fa fa-trash" aria-hidden="true"></i></a>'
                        trHTML += '<div class="card-message">'
                        trHTML += '<h5 style="text-align: center; font-size: 15px;">' + message + '</h5>'
                        trHTML += '</div>'
                        trHTML += '</div>'
                        
                        $(function() {
                            var len_fit_name = 19;

                            $('.name h4').each(function(i, obj) {
                                var name = $(this)
                                
                                var len_item_name = name.html().length;
                                if(len_fit_name < len_item_name) {
                                    var size_now = parseInt(name.css("font-size"));
                                    var size_new = size_now * len_fit_name/len_item_name;
                                    name.css("font-size",size_new);
                                }
                            })

                            var len_fit_email = 30;

                            $('.email h6').each(function(i, obj) {
                                var email = $(this)
                                
                                var len_item_email = email.html().length;
                                if(len_fit_email < len_item_email) {
                                    var size_now = parseInt(email.css("font-size"));
                                    var size_new = size_now * len_fit_email/len_item_email;
                                    email.css("font-size",size_new);
                                }
                            })
                        });
                    }

                    trHTML += '</div>';

                    object_result.append(trHTML);
                }
            }
        });
    });
})
