function writeContent() {
    event.preventDefault(true);

    var key = $('#id_message_content').val();
    var csrftoken = $('input[name="csrfmiddlewaretoken"]').attr('value');

    console.log("key = " + key);

    $.ajax({
        method: 'POST',
        url: 'http://fightcov-19-2.herokuapp.com/feedback/message/',
        dataType: 'json',
        data: JSON.stringify({
            message_content: key
        }),
        beforeSend: function (request) {
            request.setRequestHeader('X-CSRFToken', csrftoken)
        },
        success: function (response) {
            console.log(response.message_content)

            var div = document.createElement("div")

            var div1 = document.createElement("div")
            div1.classList.add("message-content")
            div1.innerText = response.no + ". " + response.message_content

            var div2 = document.createElement("div")
            div2.classList.add("message-author")
            div2.innerText = "by " + response.author + ", at " + response.created_at

            div.appendChild(div1)
            div.appendChild(div2)

            $('#real_result').append(div)
        }
    });
}
