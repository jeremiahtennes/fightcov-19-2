from django.test import TestCase, Client
from .models import Feedback, Message
from .forms import CreateFeedbackForm
from users.models import Profile
from django.contrib.auth.models import User
from django.contrib import auth

# Create your tests here.

class FeedbackTest(TestCase):
	
	def setUp(self):
		self.client = Client()
		credentials = {
			'username': 'rainer',
			'password' : 'qwerty1234',
			'email': 'rainer@gmail.com'
		}

		user = User.objects.create_user(**credentials)

		profile = Profile.objects.create(
			nama = 'Rainer Adonai',
			user = user
		)

		response = self.client.post('/users/login/', credentials, follow=True)

	def test_url_feedback_is_exist(self):
		response = self.client.get('/feedback/')
		self.assertEqual(response.status_code,200)

	def test_feedback_using_feedback_template(self):
		response = self.client.get('/feedback/')
		self.assertTemplateUsed(response, 'feedback_page.html')

	def test_if_user_not_logged_in(self):
		self.client.get('/users/logout/')
		response = self.client.get('/feedback/add-feedback')
		content = response.content.decode('utf8')
		self.assertIn('Log In', content)

	def test_if_user_already_logged_in(self):
		response = self.client.get('/feedback/add-feedback')
		content = response.content.decode('utf8')
		self.assertIn('Message:', content)
		self.assertIn('Submit', content)

	def test_if_feedback_page_return_feedback_objects(self):
		logged_in_user = auth.get_user(self.client)

		feedback = Feedback.objects.create(
			name = Profile.objects.get(user = logged_in_user).nama,
			email = logged_in_user.email,
			message = 'Hello! Keep support me'
		)

		response = self.client.get('/feedback/')
		content = response.content.decode('utf8')
		self.assertIn(feedback.message, content)
		self.assertIn(feedback.name, content)
		self.assertIn(feedback.email, content)

	def test_add_feedback_post(self):
		response = self.client.post('/feedback/add-feedback', data={"message" : "comeback stronger"})
		self.assertEqual(Feedback.objects.all().count(),1)

	def test_parse_data(self):
		response = Client().get('/feedback/parseData/Andi')
		self.assertTemplateUsed('feedback_page.html')
		self.assertEqual(response.status_code, 200)

	def test_get_data_all(self):
		response = Client().get('/feedback/getDataAll')
		self.assertTemplateUsed('feedback_page.html')
		self.assertEqual(response.status_code, 301)

	def test_parse_data_all(self):
		response = Client().get('/feedback/parseDataAll')
		self.assertTemplateUsed('feedback_page.html')
		self.assertEqual(response.status_code, 301)

	def test_if_message_bar_exists(self):
		response = Client().get('/feedback/')
		content = response.content.decode('utf8')
		self.assertIn("Put your message for our team:", content)

	def test_message_model(self):
		Message.objects.create(
			message_content = "Kalian keren!",
			message_author_username = "jonathanelia",
		)
		self.assertEqual(Message.objects.all().count(), 1)

	def test_message(self):
		response = self.client.post('/feedback/message/', data={'message_content':'nicee'}, content_type="application/json")
		self.assertTemplateUsed('feedback_page.html')
		self.assertEqual(response.status_code, 200)


class FeedbackModelTest(TestCase):

	def test_string_representation(self):
		feedback = Feedback(name='Lee Ji Eun', email='leejieun@gmail.com', message='fighting!')
		self.assertEqual(str(feedback), feedback.name + " - " + feedback.email + " - " + feedback.message)

	def test_verbose_name_plural(self):
		self.assertEqual(str(Feedback._meta.verbose_name_plural), "feedbacks")

