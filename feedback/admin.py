from django.contrib import admin
from .models import Feedback, Message

# Register your models here.
admin.site.register(Feedback)
admin.site.register(Message)