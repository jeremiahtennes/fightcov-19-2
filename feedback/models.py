from django.db import models

# Create your models here.

class Feedback(models.Model):
	name = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	message = models.TextField()

	def __str__(self):
		return "{} - {} - {}".format(self.name, self.email, self.message)

class Message(models.Model):
    message_content = models.CharField(max_length=300)
    message_author_username = models.CharField(max_length=300,default="")
    no = models.CharField(max_length=10,default="")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.message_author_username, self.message_content)
