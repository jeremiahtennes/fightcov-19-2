from django.shortcuts import render, redirect
from .forms import CreateFeedbackForm, MessageForm
from .models import Feedback, Message
from . import forms
from users.models import Profile
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
import json
import requests
from django.core import serializers

# Create your views here.
def feedback(request):

    feedback = Feedback.objects.all()
    response = {
    	'title' : 'Feedback',
        'feedback': feedback,
        'messages': Message.objects.all(),
        'message_form': MessageForm()
    }

    return render(request, 'feedback_page.html', response)

def add_feedback(request):
    response = {
        'title' : 'Add Feedback',
        'feedback_form': CreateFeedbackForm()
    }

    if request.method == "POST":
        feedback_form = CreateFeedbackForm(request.POST)
        if feedback_form.is_valid():
            feedback = feedback_form.save(commit=False)
            user = request.user
            user_profile = Profile.objects.get(user=user)
            name = user_profile.nama
            email = user.email
            feedback.name = name
            feedback.email = email
            feedback.save()
            return redirect('feedback:feedback')
    return render(request, 'add_feedback_page.html', response)

def delete_feedback(request, id):
    feedback = Feedback.objects.get(id=id)
    feedback.delete()
    return redirect('feedback:feedback')

def getData(request):
    q = request.GET['q']
    url = 'http://fightcov-19-2.herokuapp.com/feedback/parseData/' + q

    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)

def parseData(request, query):
    queryResult = []

    for feedback in Feedback.objects.all():
        if(query.lower() in feedback.name.lower()):
            queryResult.append(feedback)

    feedback_list = serializers.serialize('json', queryResult)
    return HttpResponse(feedback_list, content_type="application/json")

def getDataAll(request):
    url = 'http://fightcov-19-2.herokuapp.com/feedback/parseDataAll/'

    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)
    
def parseDataAll(request):
    feedback = Feedback.objects.all()

    feedback_list = serializers.serialize('json', feedback)
    return HttpResponse(feedback_list, content_type="application/json")

def message(request):
    if request.method == "POST":
        data = json.loads(request.body)
        form = MessageForm({'message_content' : data['message_content']})

        if form.is_valid():
            banyak = Message.objects.all().count()
            author = User.objects.get(username=request.user)
            item = form.save(commit=False)
            item.message_author_username = author.username
            item.no = str(banyak+1)
            item.save()
            print(Message.objects.last().message_content)
            return JsonResponse({
                'message_content' : Message.objects.last().message_content,
                'author' : Message.objects.last().message_author_username,
                'no' : str(banyak+1),
                'created_at' : Message.objects.last().created_at,
            })
        print(form.errors)
