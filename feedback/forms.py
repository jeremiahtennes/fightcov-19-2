from django.shortcuts import render, redirect
from django import forms
from .models import Feedback, Message
#from . import forms
from django.forms import ModelForm
# from . import models

class CreateFeedbackForm(forms.ModelForm):
	class Meta:
		model = Feedback 
		fields = [
			"message",
		]

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = [
            'message_content',
        ]

        widgets = {
        	'message_content': forms.TextInput(
        		attrs = {
					'class': 'form-control'
        		}
        	)
        }
