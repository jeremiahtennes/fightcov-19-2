#from django.contrib import admin
from . import views
from django.urls import path, include

app_name = 'feedback'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('add-feedback', views.add_feedback, name='add_feedback'),
    path('delete-feedback/<int:id>', views.delete_feedback, name='delete_feedback'),
    path('getData/', views.getData, name='getData'),
    path('parseData/<str:query>', views.parseData, name='parseData'),
    path('getDataAll/', views.getDataAll, name='getDataAll'),
    path('parseDataAll/', views.parseDataAll, name='parseDataAll'),
    path('message/', views.message, name='message')
]

