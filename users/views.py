from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from .models import Profile

# Create your views here.
def index(request):
    home = True
    return render(request, "home.html", {'home' : home})

def signup_page(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        no_hp = request.POST['phone']
        email = request.POST['email']
        tgl_lahir = request.POST['tgl_lahir']
        kota = request.POST['kota']
        username = request.POST['username']
        password = request.POST['password']

        if(not username or not password):
            messages.error(request, "Complete your data first")
            return render(request, 'signup.html')

        if User.objects.filter(username__iexact=username).exists():
            messages.error(request, "Username already exist.")
            return render(request, 'signup.html')
        else:
            user = User.objects.create_user(username=username, password=password, email=email)
            profile = Profile.objects.create(user=user, tgl_lahir=tgl_lahir, nama=nama, no_hp=no_hp, kota=kota)
            # user.profile.tgl_lahir = tgl_lahir
            # user.profile.nama = nama
            # user.profile.no_hp = no_hp
            # user.profile.kota = kota
            user.save()
            profile.save()

            # messages.success(request, "Successfully signed in.")

            user_auth = authenticate(request, username=username, password=password)
            login(request, user_auth)

            return redirect('/')
    else:
        return render(request, 'signup.html')

def login_page(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        if(not username or not password):
            messages.error(request, "Complete your data first")
            return render(request, 'login.html')

        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, "Username or password is incorrect.")
            return render(request, 'login.html')
            
    else:      
        return render(request, 'login.html')

def logout_acc(request):
    logout(request)
    # messages.success(request, "Successfully logged out.")
    return redirect('/')

