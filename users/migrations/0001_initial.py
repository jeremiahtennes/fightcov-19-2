# Generated by Django 3.1.4 on 2020-12-21 17:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tgl_lahir', models.DateField(blank=True, null=True, verbose_name='tgl lahir')),
                ('nama', models.CharField(max_length=100, null=True, verbose_name='nama')),
                ('no_hp', models.CharField(max_length=20, null=True, verbose_name='hp')),
                ('kota', models.CharField(blank=True, max_length=200, null=True, verbose_name='kota')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
