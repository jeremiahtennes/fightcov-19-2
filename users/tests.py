from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .apps import UsersConfig
import time
from .views import login_page, index, signup_page, logout_acc
from django.contrib.auth.models import User


# Create your tests here.
class UsersUnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(UsersConfig.name, 'users')
        self.assertEqual(apps.get_app_config('users').name, 'users')

    def test_login_url_exist(self):
        response = Client().get('/users/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/users/logout/')
        self.assertEqual(response.status_code, 302)

    def test_signup_url_exist(self):
        response = Client().get('/users/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_function(self):
        self.found = resolve('/users/signup/')
        self.assertEqual(self.found.func, signup_page)

    def test_login_function(self):
        self.found = resolve('/users/login/')
        self.assertEqual(self.found.func, login_page)

    def test_logout_function(self):
        self.found = resolve('/users/logout/')
        self.assertEqual(self.found.func, logout_acc)
    
    def test_login_views_valid(self):
        self.credentials = {
            'username': 'fightcov',
            'password': 'kepo123456'}
        User.objects.create_user(**self.credentials)
        # send login data
        response = self.client.post('/users/login/', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)
