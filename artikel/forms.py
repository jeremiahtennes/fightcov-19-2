from django import forms
from .models import inputan
from django.forms import ModelForm

class inputanForm(forms.ModelForm):
    class Meta:
        model = inputan
        fields = [
            'Judul',
            'Isi',
            'Link',
        ]

        widgets = {
            'Judul' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Tuliskan judul artikel',
                    'required' : True,
                }
            ),
            'Isi' : forms.TextInput(
                attrs ={
                    'class' : 'form-control',
                    'placeholder' : 'Masukan isi artikel',
                    'required' : True,
                }
            ),
            'Link' : forms.TextInput(
                attrs ={
                    'class' : 'form-control',
                    'placeholder' : 'Masukan link sumber artikel',
                }
            ),
        }