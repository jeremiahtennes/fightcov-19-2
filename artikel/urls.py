from django.urls import path
from . import views
from .views import join, inp_delete

urlpatterns = [
    path('', views.index,),
    path('FormArtikel/', views.join, name='inputan'),
    path('<int:pk>', inp_delete, name='Delete'),
]