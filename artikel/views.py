from django.shortcuts import render, redirect
from .models import inputan as tabel
from .forms import inputanForm

# Create your views here.
def index(request):
    inp = tabel.objects.all()
    return render(request, 'artikel/index.html', {'inp': inp})

def join(request):
    form = inputanForm(request.POST)
    if request.method == "POST": 
        if form.is_valid():
            inp = tabel()
            inp.Judul = form.cleaned_data['Judul']
            inp.Isi = form.cleaned_data['Isi']
            inp.Link = form.cleaned_data['Link']
            inp.save()
            return redirect('/artikel/')
    inp = tabel.objects.all()
    form = inputanForm()
    response = {"inp":inp, 'form' : form}
    return render(request,'artikel/formArtikel.html',response)

def inp_delete(request,pk):
    form = inputanForm(request.POST)
    if request.method == "POST": 
        if form.is_valid():
            inp = tabel()
            inp.Judul = form.cleaned_data['Judul']
            inp.Isi = form.cleaned_data['Isi']
            inp.Link = form.cleaned_data['Link']
            inp.save()
            return redirect('/artikel/')
    tabel.objects.filter(pk=pk).delete()
    data = tabel.objects.all()
    form = inputanForm()
    response = {"inp":data,'form':form}
    return redirect('/artikel/')